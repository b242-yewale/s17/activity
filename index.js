/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function userInfoPrompt(){
	let userFullName = prompt("Enter your full Name: ");
	let userAge = prompt("Enter your age: ");
	let userLocation = prompt("Enter your location: ");

	console.log("Hello "+userFullName);
	console.log("You are "+userAge+" years old.");
	console.log("You live in "+userLocation);
	alert("Thank you for your Input:)")

}

userInfoPrompt();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function displayFavArtists(){
	console.log("1. Porcupine Tree");
	console.log("2. Metallica");
	console.log("3. Indin Ocean");
	console.log("4. Eminem");
	console.log("5. Pink Floyd");
}

displayFavArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function favMoviesRating(){
	console.log("1. Donnie Brasco");
	console.log("Rotten Tomatoes Rating: 88%");
	console.log("2. Heat");
	console.log("Rotten Tomatoes Rating: 88%");
	console.log("3. Edward Scissorhands");
	console.log("Rotten Tomatoes Rating: 89%");
	console.log("4. Interstellar");
	console.log("Rotten Tomatoes Rating: 73%");
	console.log("5. Tenet");
	console.log("Rotten Tomatoes Rating: 69%");
}

favMoviesRating();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();  
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


// console.log(friend1); cannot access local variable
